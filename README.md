# iot-simulator

After some research, here is the approximate roadmap for this project:

1. Simulation of IoT Devices: Use AWS IoT Device Simulator to generate virtual IoT devices. Possibly custom device simulators.

2. Data Ingestion Configuration: Set up AWS IoT for data ingestion. Connect simulated IoT devices to the cloud platform.

3. Database Setup: Establish a server for SQLite to store incoming IoT data. Configure the cloud platform to feed data to the selected database.

4. Data Analytics: Implement Python-based data analytics.

5. Docker Environment Configuration: Containerize components using Docker for easier management.

6. Data Visualization: Develop a web app using HTML, CSS, and JavaScript (possibly supplemented with D3.js) for data visualization. This app will query the database and present data and results. 

7. Deployment: Docker Swarm or Kubernetes to deploy the application. 
